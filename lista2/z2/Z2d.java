import java.util.Arrays;
import java.util.Random;

public class Z2d {
  
  public static void main(String[] args) {
  	int[] n = new int[100];
	generuj(n, -999, 999);
	wypisz(n);
	System.out.println("dodatnie: "+sumaDodatnich(n));
	System.out.println("ujemne: "+sumaUjemnych(n));
  }

public static void generuj (int[] tab, int min, int max) {
	Random r = new Random();
        for (int i = 0; i < tab.length; ++i) {
            tab[i] = min+r.nextInt(max-min);
	   }
    }

public static void wypisz(int[] tab) {
        for (int i : tab) {
            System.out.print(i + " ");
        }
        System.out.println("");
}

public static int sumaDodatnich (int[] tab){
	int dod=0;
	for (int i : tab) {
            if(i>0) dod=dod+i; 
	}
	return dod;
}

public static int sumaUjemnych (int[] tab){
	int ujem=0;
	for (int i : tab) {
            if(i<0) ujem=ujem+i; 
	}
	return ujem;
}
}
