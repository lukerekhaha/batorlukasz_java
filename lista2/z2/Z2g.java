import java.util.Arrays;
import java.util.Random;

public class Z2g {
  
  public static void main(String[] args) {
  	int[] n = new int[100];
	generuj(n, -999, 999);
	wypisz(n);
	odwrocFragment(n, 30, 50);
	wypisz(n);
  }

public static void generuj (int[] tab, int min, int max) {
	Random r = new Random();
        for (int i = 0; i < tab.length; ++i) {
            tab[i] = min+r.nextInt(max-min);
	   }
    }

public static void wypisz(int[] tab) {
        for (int i : tab) {
            System.out.print(i + " ");
        }
        System.out.println("");
}

public static void odwrocFragment (int[] tab, int lewy, int prawy){
	int[] t = new int[prawy-lewy];
	int l = lewy;
	for (int i = 0; i < t.length; ++i) {
		 t[i] = tab[l];
		 l++;
	}
	for (int i = 0; i < t.length; ++i) {
		 tab[prawy] = t[i];
		 prawy--;
	}
}
}
