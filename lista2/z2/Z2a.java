import java.util.Arrays;
import java.util.Random;

public class Z2a {
  
  public static void main(String[] args) {
  	int[] n = new int[100];
	generuj(n, -999, 999);
	wypisz(n);
	System.out.println("nieparzyste: "+ileNieparzystych(n));
	System.out.println("parzyste: "+ileParzystych(n));
  }

public static void generuj (int[] tab, int min, int max) {
	Random r = new Random();
        for (int i = 0; i < tab.length; ++i) {
            tab[i] = min+r.nextInt(max-min);
	   }
    }

public static void wypisz(int[] tab) {
        for (int i : tab) {
            System.out.print(i + " ");
        }
        System.out.println("");
}

public static int ileNieparzystych (int[] tab){
	int niep=0;
	for (int i : tab) {
            if(i%2!=0) niep++; 
	}
	return niep;
}

public static int ileParzystych (int[] tab){
	int parz=0;
	for (int i : tab) {
            if(i%2==0) parz++; 
	}
	return parz;
}
}
