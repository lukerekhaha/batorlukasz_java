import java.util.Arrays;
import java.util.Random;

public class Z2f {
  
  public static void main(String[] args) {
  	int[] n = new int[100];
	generuj(n, -999, 999);
	wypisz(n);
	signum(n);
	wypisz(n);
  }

public static void generuj (int[] tab, int min, int max) {
	Random r = new Random();
        for (int i = 0; i < tab.length; ++i) {
            tab[i] = min+r.nextInt(max-min);
	   }
    }

public static void wypisz(int[] tab) {
        for (int i : tab) {
            System.out.print(i + " ");
        }
        System.out.println("");
}

public static void signum (int[] tab){
	int ujem = 0;
	int dod = 0;
	for (int i = 0; i < tab.length; ++i) {
		 if(tab[i]<=0) tab[i] = 1;
		 else tab[i] = -1; 
	}
}
}
