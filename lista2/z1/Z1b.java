import java.util.Arrays;
import java.util.Random;

public class Z1b {
  
  public static void main(String[] args) {
  	int[] n = new int[100];
	int min = -999;
	int max = 999;
	Random r = new Random();
        for (int i = 0; i < n.length; ++i) {
            n[i] = min+r.nextInt(max-min);
	   }
	for (int i : n) {
            System.out.print(i + " ");
	}
	System.out.println("");
	int ujem = 0;
	int dod = 0;
	int zer = 0;
	for (int i : n) {
            if(i==0) zer++;
		 else if(i<0) ujem++;
		 else dod++; 
	}
	System.out.println("ilosc zer: " + zer);
	System.out.println("ilosc ujemnych: " + ujem);
	System.out.println("ilosc dodatnich: " + dod);
  }
}