import pl.imiajd.bator.*;
import java.util.*;
import java.time.LocalDate;

public class Zad2{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski","Zbyszek Gogdan",LocalDate.of(1982, 3,12 ),true, 50000,LocalDate.of(2014, 12, 1));
        ludzie[1] = new Student("Nowak","Magda, Kasia",LocalDate.of(1994, 11,2 ),false, "informatyka",5);
	   for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
	   ((Student) ludzie[1]).setSredniaOcen(4.0);
	
        System.out.println(ludzie[1].getNazwisko() + ": " + ludzie[1].getOpis());
    }
}


