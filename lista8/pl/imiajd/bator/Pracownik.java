package pl.imiajd.bator;

public class Pracownik extends Osoba{
   	public Pracownik(String nazwisko,String imiona,LocalDate dataUrodzenia,boolean plec,double pobory,LocalDate dataZatrudnienia){
        super(nazwisko, imiona, dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    	}

	public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    	}
    
    	public double getPobory(){
        return pobory;
    	}

    	public String getOpis(){
        return String.format("Imiona: "+getImiona()+", data urodzenia: "+getDataUrodzenia()+", plec: "+getPlec()+", pracuje od :"+getDataZatrudnienia()+", pracownik z pensją %.2f zł",pobory);
    	}

    	private double pobory;
    	LocalDate dataZatrudnienia;
}
