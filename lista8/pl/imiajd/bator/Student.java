package pl.imiajd.bator;

public class Student extends Osoba{
    	public Student(String nazwisko,String imiona,LocalDate dataUrodzenia,boolean plec, String kierunek,double sredniaOcen){
        super(nazwisko, imiona, dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    	}

	public void setSredniaOcen(double sr){
        sredniaOcen=sr;
    	}

   	public double getSredniaOcen(){
        return sredniaOcen;
  	}

    	public String getOpis(){
	  return String.format("Imiona: "+getImiona()+", data urodzenia: "+getDataUrodzenia()+", plec: "+getPlec()+", kierunek studiów: " + kierunek+" , sierednia ocen: "+getSredniaOcen());
    	}

    	private String kierunek;
    	double sredniaOcen;
}

