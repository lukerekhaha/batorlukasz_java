package pl.imiajd.bator;

abstract class Osoba{
   	public Osoba(String nazwisko, String imiona,LocalDate dataUrodzenia,boolean plec){
        this.nazwisko = nazwisko;
        this.imiona=imiona;
        this.dataUrodzenia=dataUrodzenia;
        this.plec=plec;
    	}
    
    	public String getImiona(){
        return imiona;
    	}
    
    	public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
   	}

    	public String getPlec(){
		if (plec==true){
        	return "chlop";
   	}
	else{
		return "kobieta";
	}}

	public abstract String getOpis();
	public String getNazwisko(){
        return nazwisko;
  	}
    
	String imiona;
    	private String nazwisko;
    	LocalDate dataUrodzenia;
    	boolean plec;
}
