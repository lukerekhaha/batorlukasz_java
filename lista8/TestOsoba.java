import java.util.*;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski","Zbyszek Gogdan",LocalDate.of(1982, 3,12 ),true, 50000,LocalDate.of(2014, 12, 1));
        ludzie[1] = new Student("Nowak","Magda, Kasia",LocalDate.of(1994, 11,2 ),false, "informatyka",5);
	   for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
	   ((Student) ludzie[1]).setSredniaOcen(4.0);
	   System.out.println(ludzie[1].getNazwisko() + ": " + ludzie[1].getOpis());
     }
}

abstract class Osoba
{
    public Osoba(String nazwisko, String imiona,LocalDate dataUrodzenia,boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona=imiona;
        this.dataUrodzenia=dataUrodzenia;
        this.plec=plec;
    }
    
    public String getImiona()
    {
        return imiona;
    }
    
    public LocalDate getDataUrodzenia()
    {
        return dataUrodzenia;
    }

    public String getPlec()
    {
	   if (plec==true){
        	return "chlop";
        }
	   else{
		return "kobieta";
	   }
    }

    public abstract String getOpis();
    public String getNazwisko()
    {
        return nazwisko;
    }
    
    String imiona;
    private String nazwisko;
    LocalDate dataUrodzenia;
    boolean plec;
}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,String imiona,LocalDate dataUrodzenia,boolean plec,double pobory,LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    
    public LocalDate getDataZatrudnienia()
    {
        return dataZatrudnienia;
    }
    
    public double getPobory()
    
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("Imiona: "+getImiona()+", data urodzenia: "+getDataUrodzenia()+", plec: "+getPlec()+", pracuje od :"+getDataZatrudnienia()+", pracownik z pensją %.2f zł",pobory);
    }

    private double pobory;
    LocalDate dataZatrudnienia;
}


class Student extends Osoba
{
    public Student(String nazwisko,String imiona,LocalDate dataUrodzenia,boolean plec, String kierunek,double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }



    public void setSredniaOcen(double sr)
    {
        sredniaOcen=sr;
        
        
    }

    public double getSredniaOcen()
    {
        return sredniaOcen;
        
    }

    public String getOpis()
    {
	return String.format("Imiona: "+getImiona()+", data urodzenia: "+getDataUrodzenia()+", plec: "+getPlec()+", kierunek studiów: " + kierunek+" , srednia ocen: "+getSredniaOcen());
       
    }

    private String kierunek;
    double sredniaOcen;
}

