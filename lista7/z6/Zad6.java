import java.awt.Rectangle;
import java.util.*;

public class Zad6{
  public static void main(String[] args){
	BetterRectangle bt = new BetterRectangle(2,3);
	System.out.println("Obwod prostokata wynosi:  "+bt.getPerimeter()+" a pole: "+bt.GetArea());
	bt.setSize(20,10);
	bt.setLocation(20,10);
	System.out.println(bt.getLocation());
	System.out.println("Obwod prostokata wynosi:  "+bt.getPerimeter()+" a pole: "+bt.GetArea());
	}

static class BetterRectangle extends Rectangle{
	public BetterRectangle(int x,int y){
		setLocation(x,y);
		setSize(x,y);
	}
	
	public int getPerimeter(){
		return 2*x+2*y;
	}
	
	public int GetArea(){
		return x*y;
	}
}
}


