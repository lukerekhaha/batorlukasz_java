package pl.imiajd.bator;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{
	public BetterRectangle(int width,int height){
		super(width,height);
		super.setLocation(width,height);
		super.setSize(width,height);
	}
	
	public int getPerimeter(){
		return 2*width+2*height;
	}
	
	public int GetArea(){
		return width*height;
	}
}

