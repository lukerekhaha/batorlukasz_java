public class Student extends Osoba
{
	public Student(String nazwisko, String rokUrodzenia, String kierunek)
	{
		super(nazwisko, rokUrodzenia);
		this.kierunek = kierunek;
	}
	
	public String getKierunek()
	{
		return kierunek;
	}

	public void pokaz()
	{
		System.out.print("Nazwisko: " + nazwisko + ", Rok urodzenia: " + rokUrodzenia);
		System.out.println(", Kierunek: " + kierunek);
	}
	
	private String kierunek;
}

