public class Nauczyciel extends Osoba
{
	public Nauczyciel(String nazwisko, String rokUrodzenia, int pensja)
	{
		super(nazwisko, rokUrodzenia);
		this.pensja = pensja;
	}
	
	public String getPensja()
	{
		return pensja;
	}

	public void pokaz()
	{
		System.out.print("Nazwisko: " + nazwisko + ", Rok urodzenia: " + rokUrodzenia);
		System.out.println(", Pensja: " + pensja);
	}
	
	private int pensja;
}
