package pl.imiajd.bator;

public class Osoba
{
	public Osoba(String nazwisko, String rokUrodzenia)
	{
		this.nazwisko =  nazwisko;
		this.rokUrodzenia = rokUrodzenia;
	}
	
	public String getNazwisko()
	{
		return nazwisko;
	}

	public String getRokUrodzenia()
	{
		return rokUrodzenia;
	}

	public void pokaz()
	{
		System.out.print("Nazwisko: " + nazwisko + ", Rok urodzenia: " + rokUrodzenia);
	}
	
	private String nazwisko;
	private String rokUrodzenia;
}
