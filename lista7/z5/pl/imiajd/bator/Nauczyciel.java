package pl.imiajd.bator;

public class Nauczyciel extends Osoba
{
	public Nauczyciel(String nazwisko, String rokUrodzenia, int pensja)
	{
		super(nazwisko, rokUrodzenia);
		this.pensja = pensja;
	}
	
	public int getPensja()
	{
		return pensja;
	}

	public void pokaz()
	{
		super.pokaz();
		System.out.println(", Pensja: " + pensja);
	}
	
	private int pensja;
}
