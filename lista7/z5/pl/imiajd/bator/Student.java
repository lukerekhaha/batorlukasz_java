package pl.imiajd.bator;

public class Student extends Osoba
{
	public Student(String nazwisko, String rokUrodzenia, String kierunek)
	{
		super(nazwisko, rokUrodzenia);
		this.kierunek = kierunek;
	}
	
	public String getKierunek()
	{
		return kierunek;
	}

	public void pokaz()
	{
		super.pokaz();
		System.out.println(", Kierunek: " + kierunek);
	}
	
	private String kierunek;
}

