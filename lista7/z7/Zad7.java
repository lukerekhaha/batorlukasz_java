import java.awt.Rectangle;
import java.util.*;

public class Zad7{
  public static void main(String[] args){
	BetterRectangle bt=new BetterRectangle(2,3);
	System.out.println("Obwod prostokata wynosi:  "+bt.getPerimeter()+" a pole: "+bt.GetArea());
	bt.setSize(20,10);
	bt.setLocation(20,10);
	System.out.println(bt.getLocation());
	System.out.println("Obwod prostokata wynosi:  "+bt.getPerimeter()+" a pole: "+bt.GetArea());
	}

static class BetterRectangle extends Rectangle{
  	public BetterRectangle(int width,int height){
		super(width,height);
		super.setLocation(width,height);
		super.setSize(width,height);
	}
	
	public int getPerimeter(){
		return 2*width+2*height;
	}
	
	public int GetArea(){
		return width*height;
	}
}
}

