import java.util.*;
import pl.imiajd.bator.Osoba;
import java.time.LocalDate;


public class Zad1{
  public static void main(String[] args){ 
	Osoba[] grupa = new Osoba[5];
	grupa[0]= new Osoba("Msada",LocalDate.of(1970, 1, 1));
	grupa[1]= new Osoba("Msada",LocalDate.of(1973, 12, 1));
	grupa[2]= new Osoba("Malek",LocalDate.of(1994, 11, 28));
	grupa[3]= new Osoba("Komor",LocalDate.of(1960, 12, 1));
	grupa[4]= new Osoba("Kukiz",LocalDate.of(1960, 12, 1));

	for(int y=0;y<grupa.length;y++){
		System.out.println(grupa[y].toString());
	}
	Arrays.sort(grupa);
	System.out.println();
	System.out.println("Wynik porownania compareTo: "+grupa[1].compareTo(grupa[2])+ " equals 'rowne': "+grupa[1].equals(grupa[2]));
	System.out.println("Po sortowaniu: ");
	System.out.println();
	for(int y=0;y<grupa.length;y++){
		System.out.println(grupa[y].toString());
	}
  }
}
